#!/usr/bin/env python3

from setuptools import setup, find_packages

import versioneer

setup(name='taxcalc',
      version=versioneer.get_version(),
      cmdclass=versioneer.get_cmdclass(),
      description='calculate taxes',
      author='Timothy B. Hartman',
      packages=find_packages('src'),
      package_dir = {'':'src'},
      entry_points={
          'console_scripts':[
              'taxcalc = taxcalc.__main__:main',
              ],
          },
      install_requires = ['argparse',
                          ],
      extras_require = {
          },
      classifiers = [
          'Development Status :: 3 - Alpha',
          'Programming Language :: Python :: 3',
          ]
     )
