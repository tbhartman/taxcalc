#!/usr/bin/env python3
import time
t0 = time.clock()
import collections
import sys
import copy
import enum
import logging
import datetime
import math

import coverage
import doctest
import importlib
import pathlib
import unittest
import textwrap
import io

cov = coverage.Coverage(branch=True)
cov.start()

taxes = importlib.import_module('.taxes', 'taxcalc')
manna = importlib.import_module('.manna', 'taxcalc')
tests = importlib.import_module('.tests', 'taxcalc')
importlib.reload(taxes)
importlib.reload(manna)

_tests = []

def addtest(cls):
    _tests.append(cls)
    return cls

class f1040test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.personal = None
        cls.data = None
        cls.form = None

    @property
    def personal(self):
        return self.__class__._p
    @personal.setter
    def personal(self, value):
        self.__class__._p = value
    @property
    def data(self):
        return self.__class__._data
    @data.setter
    def data(self, value):
        self.__class__._data = value
    @property
    def form(self):
        return self.__class__._form
    @form.setter
    def form(self, value):
        self.__class__._form = value

    def test_01_setup(self):
        self.personal = PersonalData(
                name = ('John','Middle','Doe'),
                status = FilingStatus.JOINT,
                )
        w2 = W2(self.personal,
                label='a W2',
                data = {1: 1000.00,
                        2:  100.00,
                        },
                )
        self.data = [w2]
        self.form = f1040(self.personal, data=[w2])

    def test_02_lines_07(self): self.assertEqual(self.form[7], 1000)
    def test_02_lines_08a(self): self.assertEqual(self.form[(8,'a')], 0)
    def test_02_lines_22(self): self.assertEqual(self.form[22], 1000)

    def test_03_report(self):
        string = io.StringIO()
        report(self.personal,
               self.data,
               [f1040],
               width=20,
               file=string)
        # string.seek(0)
        # self.assertMultiLineEqual(string.read(),textwrap.dedent("""\
        #         --------------------
        #                f1040
        #         --------------------
        #         ___ [ 7]    $1000.00
        #         ___ [ 8]
        #         ___ [10]
        #         ___ [22]    $1000.00
        #         """))

class taxtabletest(unittest.TestCase):
    def _assertTaxEqual(self, low, high, value):
        lowtax = taxtable(low, FilingStatus.JOINT)
        hightax = taxtable(high, FilingStatus.JOINT)
        self.assertLessEqual(lowtax,value)
        self.assertGreaterEqual(hightax,value)

    def test_01(self): self._assertTaxEqual(1000,1025,101)
    def test_02(self): self._assertTaxEqual(2000,2025,201)
    def test_03(self): self._assertTaxEqual(9000,9050,903)
    def test_04(self): self._assertTaxEqual(19000,19050,1926)
    def test_05(self): self._assertTaxEqual(27000,27050,3126)
    def test_06(self): self._assertTaxEqual(45000,45050,5826)
    def test_07(self): self._assertTaxEqual(63000,63050,8526)
    def test_08(self): self._assertTaxEqual(74000,74050,10176)
    def test_09(self): self._assertTaxEqual(74000,74050,10176)
    def test_10(self): self._assertTaxEqual(87800,87850,13499)

    def test_otherstatus(self):
        self.assertRaises(
                NotImplementedError,
                taxtable,
                1,
                FilingStatus.SINGLE)

    def test_overlimit(self):
        self.assertRaises(
                NotImplementedError,
                taxtable,
                1000000,
                FilingStatus.JOINT)

@addtest
class MannaTests(unittest.TestCase):
    """
    >>> M = manna.Manna
    >>> e = M.evaluate
    >>> A = manna.MannaReport.ascii
    >>> e(M(1))
    1
    >>> M(1,2)
    Traceback (most recent call last):
        ...
    TypeError:...
    >>> e((M(2) + 3))
    5
    >>> e((2 + M(2)))
    4
    >>> e((M(5) - 2))
    3
    >>> e((5 - M(1)))
    4
    >>> round(e(3 * M(1.1) * 2),3)
    6.6


    >>> A(M())
    ''
    >>> A(M(0) - 3)
    '0 - 3'

    >>> manna.Round(M(2.2)).evaluate()
    2

    """

@addtest
class TaxMoneyTests(unittest.TestCase):
    """
    >>> a = taxes.TaxItem("test description", taxes.USD(1))
    >>> a
    <TaxItem USD 1.00>
    >>> a.value + taxes.USD(1)
    USD 2

    """

def test(verbose): # pragma: no cover

    print('Running tests...')

    # unit tests
    failures = set()
    result = unittest.TestResult()
    suite = unittest.TestSuite()
    list(map(suite.addTests,
             map(unittest.TestLoader().loadTestsFromTestCase, _tests)))
    for tc in suite:
        name = '{}.{}'.format(tc.__class__.__name__, tc._testMethodName)
        if verbose:
            print(name + '...\r', end='')
        result = tc.run(result)
        new_failures = set(result.failures) - failures
        new_errors = set(result.errors) - failures
        if new_failures or new_errors:
            if new_failures:
                failures.update(new_failures)
                _, error = new_failures.pop()
                text = 'FAILURE'
            else:
                failures.update(new_errors)
                _, error = new_errors.pop()
                text = 'ERROR'
            if verbose:
                print(name + '...' + text)
            else:
                print('{}: {}'.format(text, name))
            print(textwrap.indent(error.rstrip(), ' '*4))
        else:
            if verbose:
                print(name + '...passed')
    print('{:d} of {:d} unit tests failed'.format(
        len(result.failures + result.errors), result.testsRun))

    # doc tests
    flags = ( doctest.DONT_ACCEPT_TRUE_FOR_1
              | doctest.ELLIPSIS
              | doctest.REPORT_UDIFF )
    mods = [taxes, manna, tests]
    fail = 0
    total = 0
    for i in mods:
        ifail, itotal = doctest.testmod(i, verbose=verbose, optionflags=flags)
        fail += ifail
        total += itotal
    print('{:d} of {:d} doc tests failed'.format(fail, total))

    cov.stop()
    path = pathlib.Path(__file__).parent / 'taxes.coverage'
    covered = cov.html_report(directory=str(path), omit=['*tests.py']) / 100
    print('coverage: {:6.1%}'.format(covered))

    print('tests run in {:.2f}s'.format(time.clock()-t0))


