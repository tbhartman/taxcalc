#!/usr/bin/env python3
import pathlib
import importlib.machinery
_thispath = pathlib.Path(__file__).parent
_moneypath = _thispath/'third_party'/'money'/'money'/'__init__.py'

_moneylib_loader = importlib.machinery.SourceFileLoader('money',str(_moneypath))
_moneylib = _moneylib_loader.load_module()


# populate this namespace
Money = _moneylib.Money

